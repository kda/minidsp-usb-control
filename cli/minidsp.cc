#include <iostream>
#include <unordered_map>

#include <libgen.h>
#include <string.h>

#include "minidsp_control.h"

static std::unordered_map<MinidspControl::Source, std::string> SourceToString = {
  { MinidspControl::SOURCE_TOSLINK, "toslink" },
  { MinidspControl::SOURCE_SPDIF, "spdif" },
  { MinidspControl::SOURCE_UNKNOWN, "unknown" },
};

std::pair<std::string, bool> SourceEnumToString(MinidspControl::Source source) {
  auto found = SourceToString.find(source);
  if (found != SourceToString.end()) {
    return {found->second, true};
  }
  return {"unknown", false};
}

std::pair<MinidspControl::Source, bool> SourceStringToEnum(std::string& sourceName) {
  for (const auto& sourceString : SourceToString) {
    if (sourceName == sourceString.second) {
      return {sourceString.first, true};
    }
  }
  return {MinidspControl::SOURCE_UNKNOWN, false};
}

int main(int iArgc, char* szArgv[])
{
	if (iArgc == 1) {
    std::cout << "Usage: " << basename(szArgv[0]) << " <command [value]>" << std::endl;
    std::cout << "    commands: " << std::endl;
    std::cout << "              volume [level]" << std::endl;
    std::cout << "              source [(spdif|toslink)]" << std::endl;
    std::cout << "              config [1234]" << std::endl;
    std::cout << "              mute [(enable|disable)]" << std::endl;
    std::cout << "              mvm [(disable|irmode)]" << std::endl;
    return -1;
	}

  std::unique_ptr<MinidspControl> control(new MinidspControl);

  if (control->isConnected() == false) {
    std::cout << "Error: Unable to connect to Minidsp" << std::endl;
    return -1;
  }

  int retval = 0;

  if (strcmp(szArgv[1], "volume") == 0) {
    int volume = control->getVolume();
    std::cout << "volume: " << volume << std::endl;
    if (iArgc > 2) {
      int newVolumeLevel = atoi(szArgv[2]);
      control->setVolume(newVolumeLevel);
      volume = control->getVolume();
      std::cout << "volume: " << volume << std::endl;
    }
  } else if (strcmp(szArgv[1], "source") == 0) {
    MinidspControl::Source source = control->getSource();
    std::cout << "source: " << SourceEnumToString(source).first << std::endl;
    if (iArgc > 2) {
      MinidspControl::Source newSource;
      if (strcmp(szArgv[2], "toslink") == 0) {
        newSource = MinidspControl::SOURCE_TOSLINK;
      } else if (strcmp(szArgv[2], "spdif") == 0) {
        newSource = MinidspControl::SOURCE_SPDIF;
      } else {
        std::cerr << "unrecognized source: " << szArgv[2] << std::endl;
        return -1;
      }
      control->setSource(newSource);
      source = control->getSource();
      std::cout << "source: " << SourceEnumToString(source).first << std::endl;
    }
  } else if (strcmp(szArgv[1], "config") == 0) {
    int config = control->getConfig() + 1;
    std::cout << "config: " << config << std::endl;
    if (iArgc > 2) {
      int newConfig;
      newConfig = atoi(szArgv[2]) - 1;
      if (newConfig < 0 || newConfig > 3) {
        std::cerr << "invalid config: " << szArgv[2] << std::endl;
        return -1;
      }
      control->setConfig(newConfig);
      config = control->getConfig() + 1;
      std::cout << "config: " << config << std::endl;
    }
  } else if (strcmp(szArgv[1], "mute") == 0) {
    bool mute = control->getMute();
    std::cout << "mute: " << mute << std::endl;
    if (iArgc > 2) {
      bool enabled = strcmp(szArgv[2], "enabled") == 0;
      control->setMute(enabled);
      mute = control->getMute();
      std::cout << "mute: " << mute << std::endl;
    }
  } else {
    std::cout << "ERROR: unknown command '" << szArgv[1] << "'" << std::endl;
    retval = -1;
  }

	return retval;
}

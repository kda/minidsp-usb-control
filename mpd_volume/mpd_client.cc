#include "mpd_client.h"
#include <mpd/connection.h>
#include <mpd/error.h>

#include <iostream>

MpdClient::MpdClient(): 
  MpdClient("localhost", 6600) {
}

MpdClient::MpdClient(const std::string& hostname, unsigned port)
: hostname_(hostname), port_(port) {
  connect();
}

MpdClient::~MpdClient() {
  disconnect();
}

bool MpdClient::isConnected() {
  return connection_ != NULL;
}

mpd_idle MpdClient::idle() {
  if (connect() == false) {
    return static_cast<mpd_idle>(0);
  }

  mpd_connection_set_timeout(connection_, 5000);
  mpd_send_idle(connection_);
  auto retval = mpd_recv_idle(connection_, false);
  auto error = mpd_connection_get_error(connection_);
  if (error != MPD_ERROR_SUCCESS) {
    //std::cerr << "mpd_recv_idle failed: " << mpd_connection_get_error_message(connection_) << std::endl;
    mpd_connection_clear_error(connection_);
    error = mpd_connection_get_error(connection_);
    if (error != MPD_ERROR_SUCCESS) {
      //std::cerr << "mpd_recv_idle fatal error: " << mpd_connection_get_error_message(connection_) << std::endl;
      disconnect();
    }
    return static_cast<mpd_idle>(0);
  }
  return retval;
}

int MpdClient::volume() {
  if (status() == false) {
    return -1;
  }
  return mpd_status_get_volume(status_);
}

bool MpdClient::connect() {
  if (connection_ != NULL) {
    return true;
  }

  connection_ = mpd_connection_new(hostname_.c_str(), port_, 0);
  if (connection_ == NULL) {
    std::cerr << "mpd_connection_new failed: " << hostname_ << ":" << port_ << std::endl;
  } else {
    //std::cerr << "mpd_connection_new succeeded: " << hostname_ << ":" << port_ << std::endl;
  }
  return isConnected();
}

void MpdClient::disconnect() {
  freeStatus();
  if (connection_ != NULL) {
    //std::cerr << "calling mpd_connection_free" << std::endl;
    mpd_connection_free(connection_);
    connection_ = NULL;
  }
}

bool MpdClient::status() {
  freeStatus();
  status_ = mpd_run_status(connection_);
  if (status_ == NULL) {
    std::cerr << "mpd_run_status failed: " << mpd_connection_get_error_message(connection_) << std::endl;
    return false;
  }
  return true;
}

void MpdClient::freeStatus() {
  if (status_ != NULL) {
    mpd_status_free(status_);
    status_ = NULL;
  }
}

#ifndef __MPD_CLIENT_H__INCLUDED__
#define __MPD_CLIENT_H__INCLUDED__

#include <string>

#include <mpd/client.h>

class MpdClient {
 public:
  MpdClient();
  MpdClient(const std::string& hostname, unsigned port);
  ~MpdClient();
  bool isConnected();

  mpd_idle idle();
  int volume();


private:
  std::string hostname_;
  unsigned port_;
  struct mpd_connection* connection_ = NULL;
  struct mpd_status* status_ = NULL;

  bool connect();
  void disconnect();
  bool status();
  void freeStatus();
};

#endif  /*  __MPD_CLIENT_H__INCLUDED__  */

#include <iostream>
#include <memory>
#include <math.h>

#include "minidsp_control.h"
#include "mpd_client.h"

bool adjustVolume(MpdClient* client) {
  std::unique_ptr<MinidspControl> control(new MinidspControl);
  if (control->isConnected() == false) {
    std::cerr << "unable to connect to minidsp: " << std::endl;
    return false;
  }
  int volumePercent = client->volume();
  std::cerr << "client->volume: " << volumePercent << std::endl;
  int volumeSteps = 255;
  if (volumePercent == 0) {
    volumeSteps = 255;
  } else {
    // TODO: convert from percent to dB
    double db = 20.0 * log10(static_cast<double>(volumePercent) / 100.0);
    volumeSteps = static_cast<int>(abs(floor((256.0 * db) / 96.0)));
  }
  std::cerr << "volumeSteps: " << volumeSteps << std::endl;
  control->setVolume(volumeSteps);
  int newVolume = control->getVolume();
  std::cerr << "control->getVolume(): " << newVolume << std::endl;
  return newVolume == volumeSteps;
}

int main(int iArgc, char* szArgv[])
{
#ifdef kda_COMMENTED_OUT
  std::unique_ptr<MinidspControl> control(new MinidspControl);
  //std::cerr << "control: " << bool(control) << std::endl;
  //std::cerr << "control->isConnected: " << control->isConnected() << std::endl;
#endif /* kda_COMMENTED_OUT */

  std::unique_ptr<MpdClient> client(new MpdClient);
  //std::cerr << "client: " << bool(client) << std::endl;
  //std::cerr << "client->isConnected: " << client->isConnected() << std::endl;

  adjustVolume(client.get());

  while (true) {
    auto idle_mask = client->idle();
    //std::cerr << "client->idle: " << idle_mask << std::endl;
    if ((idle_mask & MPD_IDLE_MIXER) != 0) {
      if (adjustVolume(client.get()) == false) {
        return -1;
      }
    }
  }

  return 0;
}

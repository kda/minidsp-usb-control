#ifndef __MINIDSP_CONTROL_H__INCLUDED__
#define __MINIDSP_CONTROL_H__INCLUDED__

#include "libusb-1.0/libusb.h"

#include <memory>

class MinidspControl {
public:
  MinidspControl();
  ~MinidspControl();
  bool isConnected();
  int getVolume();
  void setVolume(int volume);

  enum Source {
    SOURCE_UNKNOWN = -1,
    SOURCE_TOSLINK = 1,
    SOURCE_SPDIF = 0,
  };
  Source getSource();
  void setSource(Source source);
  int getConfig();
  void setConfig(unsigned char config);
  bool getMute();
  void setMute(bool enable);


private:
  libusb_context* context_ = NULL;
  libusb_device_handle* handle_ = NULL;

  bool initialize();
  void deinitialize();
  std::unique_ptr<unsigned char> prepBuffer(unsigned char* data, int length);
  void printData(unsigned char* data, int length);
  bool sendMessage(unsigned char* data, int length);
  int receiveMessage(unsigned char* expected, int offset, int length, int index);
};

#endif  /*  __MINIDSP_CONTROL_H__INCLUDED__  */

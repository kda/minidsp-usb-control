#include "minidsp_control.h"

#include <iomanip>
#include <iostream>
#include <string.h>

MinidspControl::MinidspControl() {
  initialize();
}

MinidspControl::~MinidspControl() {
  deinitialize();
}

bool MinidspControl::isConnected() {
  return context_ != NULL && handle_ != NULL;
}

int MinidspControl::getVolume() {
  unsigned char out[] = {0x05, 0xff, 0xda, 0x10};
  sendMessage(out, sizeof(out));
  return receiveMessage(out, 0, 3, 4);
}

void MinidspControl::setVolume(int volume) {
  unsigned char out[] = {0x42, 0x00};
  out[1] = static_cast<unsigned char>(volume);
  sendMessage(out, sizeof(out));
  receiveMessage(NULL, 0, 0, 0);
}

void MinidspControl::setSource(Source source) {
  unsigned char out[] = {0x34, 0x00};
  out[1] = source;
  sendMessage(out, sizeof(out));
  receiveMessage(NULL, 0, 0, 0);
}

MinidspControl::Source MinidspControl::getSource() {
  unsigned char out[] = {0x05, 0xff, 0xd9, 0x10};
  sendMessage(out, sizeof(out));
  return static_cast<Source>(receiveMessage(out, 0, 3, 4));
}

void MinidspControl::setConfig(unsigned char config) {
  unsigned char out[] = {0x25, 0x00, 0x01};
  out[1] = config;
  sendMessage(out, sizeof(out));
  unsigned char expected[] = {0xab};
  receiveMessage(expected, 0, 1, 0);

  unsigned char out2[] = {0x05, 0xff, 0xda, 0x02};
  sendMessage(out2, sizeof(out2));
  receiveMessage(out2, 0, 3, 0);
}

int MinidspControl::getConfig() {
  unsigned char out[] = {0x05, 0xff, 0xd8, 0x10};
  sendMessage(out, sizeof(out));
  return receiveMessage(out, 0, 3, 4);
}

void MinidspControl::setMute(bool enable) {
  unsigned char out[] = {0x17, 0x00};
  out[1] = enable ? 1 : 0;
  sendMessage(out, sizeof(out));
  receiveMessage(NULL, 0, 0, 0);
}

bool MinidspControl::getMute() {
  unsigned char out[] = {0x05, 0xff, 0xdb, 0x10};
  sendMessage(out, sizeof(out));
  return receiveMessage(out, 0, 3, 4) != 0;
}

bool MinidspControl::initialize()
{
  if (isConnected()) {
    return true;
  }

  int result = libusb_init(&context_);
  if (result != LIBUSB_SUCCESS) {
    std::cerr << "libusb_init failed: " << result << std::endl;
    context_ = NULL;
    return false;
  }

  handle_ = libusb_open_device_with_vid_pid(context_, 0x04d8, 0x003f);
  if (handle_ == NULL) {
    std::cerr << "libusb_open_device_with_vid_pid failed" << std::endl;
    return false;
  }

  result = libusb_kernel_driver_active(handle_, 0);
  //std::cerr << "libusb_kernel_driver_active result: " << result << std::endl;

  result = libusb_set_auto_detach_kernel_driver(handle_, 1);
  //std::cerr << "libusb_set_auto_detach_kernel_driver result: " << result << std::endl;

  result = libusb_claim_interface(handle_, 0);
  //std::cerr << "libusb_claim_interface result: " << result << std::endl;

  result = libusb_kernel_driver_active(handle_, 0);
  //std::cerr << "libusb_kernel_driver_active result: " << result << std::endl;

  return true;
}

void MinidspControl::deinitialize()
{
  if (context_ != NULL) {
    if (handle_ != NULL) {
      //int result;
      libusb_release_interface(handle_, 0);
      //result = libusb_release_interface(handle_, 0);
      //std::cerr << "libusb_release_interface result: " << result << std::endl;

      libusb_kernel_driver_active(handle_, 0);
      //result = libusb_kernel_driver_active(handle_, 0);
      //std::cerr << "libusb_kernel_driver_active result: " << result << std::endl;

      libusb_close(handle_);
      handle_ = NULL;
    }
    libusb_exit(context_);
    context_ = NULL;
  }
}

std::unique_ptr<unsigned char> MinidspControl::prepBuffer(unsigned char* data, int length) {
  std::unique_ptr<unsigned char> buffer(new unsigned char[length + 2]);

  *(buffer.get()) = (unsigned char)(length + 1);
  memcpy(buffer.get() + 1, data, length);

  unsigned char sum = 0;
  auto ucPtr = buffer.get();
  for (int index = 0; index <= length; index++, ucPtr++) {
    sum += *ucPtr;
  }
  sum &= 0xff;  // insurance
  *(buffer.get() + length + 1) = sum;

  return buffer;
}

void MinidspControl::printData(unsigned char* data, int length) {
  return;
#ifdef kda_COMMENTED_OUT
  auto ucPtr = data;
  for (int index = 0; index < length; ) {
    std::cout << std::setfill('0') << std::hex << std::setw(2) << index << ":";
    for (int col = 0; col < 16 && index < length; col++, index++, ucPtr++) {
      if (col % 8 == 0)
        std::cout << " ";
      std::cout << std::setfill('0') << std::hex << std::setw(2) << (unsigned int)*ucPtr << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::dec;
#endif /* kda_COMMENTED_OUT */
}


bool MinidspControl::sendMessage(unsigned char* data, int length) {
  if (isConnected() == false) {
    return false;
  }

  auto buffer(prepBuffer(data, length));
  printData(buffer.get(), length + 2);

  int result;
  int sent;
  result = libusb_interrupt_transfer(handle_, LIBUSB_ENDPOINT_OUT | 0x01, buffer.get(), length + 2, &sent, 5000);
  if (result != 0) {
    std::cerr << "libusb_interrupt_transfer failed: " << result << " - " << libusb_error_name(result) << std::endl;
    return false;
  }
  //std::cout << "libusb_interrupt_transfer sent: " << sent << std::endl;

  return true;
}

int MinidspControl::receiveMessage(unsigned char* expected, int offset, int length, int index) {
  if (isConnected() == false) {
    return -1;
  }

  // TODO: figure out size from libusb (64 is best guess, wireshark indicates 96)
  unsigned char dataIn[64];
  while (true) {
    int result;
    memset(dataIn, 0xff, 64);
    int received;
    result = libusb_interrupt_transfer(handle_, LIBUSB_ENDPOINT_IN | 0x01, dataIn, 64, &received, 5000);
    if (result != 0) {
      std::cerr << "libusb_interrupt_transfer failed: " << result << " - " << libusb_error_name(result) << std::endl;
      return -1;
    }
    //std::cout << "libusb_interrupt_transfer received: " << received << std::endl;
    printData(dataIn, 64);

    if (expected != NULL) {
      // TODO: validate length
      auto dataInPtr = dataIn + offset + 1;
      auto expectedPtr = expected + offset;
      bool match = true;
      for (int idx = 0; idx < length; idx++, dataInPtr++, expectedPtr++) {
        if (*dataInPtr != *expectedPtr) {
          match = false;
          break;
        }
      }
      if (match) {
        break;
      }
    } else {
      break;
    }
  }
  return dataIn[index];
}
